// Подключение модуей и конфига
var TelegramBot = require('node-telegram-bot-api');
var config = require("./config.json");
var mv = require('mv');
var fs = require("fs");

// Включение бота и отключение получения обновлений
var bot = new TelegramBot(config.token, {
  polling: false
});

// Функция для перемещения файла из папки контента в папку с отправленными файлами
function move(file){
	mv("./" + config.folder + "/" + file, "./" + config.folder_sent + "/" + file, function(err) {
		console.log("Файл " + file + " отправлен и перемещен в папку " + config.folder_sent);
	});	
}

// Методом setInterval делаем чтобы наш код выполнялся каждые N секунд
setInterval(function() {
	// Получаем список фалов из папки с контентом
	fs.readdir("./" + config.folder, (err, files) => {
		// Делаем проверку на длинну массива с файлами. Если он не равен нулю - продолжаем
		if(files.length){
			// Создадим пару переменных для удобства
			let text = {"caption":config.text};
			let file = "./" + config.folder + "/" + files[0];
			// Берем первый файл и смотрим его расширение
			// Если оно равно "gif" - отправляем файл как документ и перемещаем его в папку с отправленными
			if(files[0].substr(files[0].length - 3) == "gif"){
				bot.sendDocument(config.chat_id, file, text ).then(function() {
					move(files[0]);
				});
			// В других случаях обрабатываем файлы как картинки и отправляем их в таком же формате и тоже перемещаем
			}else{
				bot.sendPhoto(config.chat_id, file, text).then(function() {
					move(files[0]);
				});
			}
		// Если файлов в папке нет - оповещаем об этом администратора путем личного сообщения в телеграме и пишем в консоль
		}else{
			let error = "Контент закончился!";
			bot.sendMessage(config.admin_id,error);
			console.log(error);
		}
	});
// Это задержка для метода, секунды берутся из конфига и умножаются на 1000 для получения миллисекунд, которые и нужно передать в метод
}, config.time*1000);
